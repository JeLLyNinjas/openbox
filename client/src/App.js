import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>
            Ultimate Hero Exam!
          </h1>
        </header>
        <div className="join-room">
          <label>NAME</label>
          <input type="text"></input>
          <label>CODE</label>
          <input type="text"></input>
          <button>JOIN</button>
        </div>
      </div>
    );
  }
}

export default App;
