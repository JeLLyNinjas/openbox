package config

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

const (
	defaultConfigLocation = "./config.yml"
)

type LoggingConfig struct {
	Logging struct {
		Filepath string `yaml:"file_path"`
		LogLevel string `yaml:"log_level"`
		Format   string `yaml:"format"`
	}
}

type ServerConfig struct {
	Server struct {
		Port int `yaml:"port"`
	}
}

func openConfFile(confLocation string) ([]byte, error) {
	if confLocation == "" {
		confLocation = defaultConfigLocation
	}
	if _, err := os.Stat(defaultConfigLocation); os.IsNotExist(err) {
		return []byte(""), err
	}
	dat, err := ioutil.ReadFile(confLocation)

	return dat, err
}

func GetServerConfig(confLocation string) (ServerConfig, error) {
	dat, err := openConfFile(confLocation)

	var conf ServerConfig
	if err != nil {
		return conf, err
	}

	err = yaml.Unmarshal(dat, &conf)
	return conf, err
}

func GetLoggingConfig(confLocation string) (LoggingConfig, error) {
	dat, err := openConfFile(confLocation)

	var conf LoggingConfig
	if err != nil {
		return conf, err
	}

	err = yaml.Unmarshal(dat, &conf)
	return conf, err
}
