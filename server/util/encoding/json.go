package encoding

import (
	"encoding/json"

	log "github.com/sirupsen/logrus"
)

var logFields = log.WithFields(log.Fields{
	"package:": "encoding",
})

type GenericMessageReceive struct {
	Api     string
	Payload json.RawMessage
}

type GenericMessageSend struct {
	Api     string
	Payload interface{}
}

func parseGenericMessage(
	genericMessage []byte,
) (
	string, //Api
	json.RawMessage, //Payload
	error,
) {
	var receive GenericMessageReceive
	err := json.Unmarshal(genericMessage, &receive)
	if err != nil {
		logFields.Errorf("error unmarshalling generic message: %v", err)
		return "", nil, err
	}
	return receive.Api, receive.Payload, nil
}
