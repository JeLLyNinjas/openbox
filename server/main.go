package main

import (
	"flag"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"gitlab.com/JeLLyNinjas/openbox/server/socket"
	"gitlab.com/JeLLyNinjas/openbox/server/util/config"
)

var logFields *log.Entry

func init() {
	log.SetFormatter(&log.TextFormatter{})

	log.SetOutput(os.Stdout)

	log.SetLevel(log.DebugLevel)

	logFields = log.WithFields(log.Fields{
		"topic": "main",
	})

}

var argsAddr = flag.String("addr", "", "optional config override http service address")
var argsLogPath = flag.String("logpath", "", "optional config override of log path")

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Printf("url: %v", r.URL)
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

func setRoutes(r *mux.Router) {
	manager := socket.NewRoomManager()
	go manager.Run()
	http.HandleFunc("/ws/", func(w http.ResponseWriter, r *http.Request) {
		socket.ServeWs(manager, w, r)
	})

	r.HandleFunc("/chat/{id}", serveHome)
	http.Handle("/", r)
}

func getPort() string {
	var retPort string
	if *argsAddr == "" {
		serverConf, err := config.GetServerConfig(*argsLogPath)
		if err != nil {
			logFields.Errorf("unable to read config: %v", err)
			logFields.Infof("defaulting to port :8080")
			retPort = ":8080"
		} else {
			retPort = ":" + strconv.Itoa(serverConf.Server.Port)
		}
	}
	return retPort
}

func serveHttp() error {
	port := getPort()
	logFields.Infof("serving server on port %v", port)
	err := http.ListenAndServe(port, nil)
	return err
}

func main() {
	logFields.Infof("starting server...")
	flag.Parse()
	r := mux.NewRouter()
	setRoutes(r)
	err := serveHttp()
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
