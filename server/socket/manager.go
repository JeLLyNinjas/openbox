package socket

import (
	log "github.com/sirupsen/logrus"
)

// RoomManager creates and maintains the set of active rooms.
type RoomManager struct {
	rooms map[RoomId]*Room

	register   chan *Room
	unregister chan *Room
}

// NewRoomManager creates a new RoomManager
func NewRoomManager() *RoomManager {
	return &RoomManager{
		rooms:      make(map[RoomId]*Room),
		register:   make(chan *Room),
		unregister: make(chan *Room),
	}
}

// Run should be called as a go routine. It will manage the registering and
// unregistering of rooms
func (m *RoomManager) Run() {
	for {
		select {
		case room := <-m.register:
			log.Infof("registering room: %v", room.roomId)
			m.rooms[room.roomId] = room
		case room := <-m.unregister:
			if _, ok := m.rooms[room.roomId]; ok {
				delete(m.rooms, room.roomId)
				close(room.toRoom)
			}
		}
	}
}
