package socket

import (
	"net/http"
)

type RoomId string

type RoomState int

// Game states enums
const (
	WaitingForPlayers RoomState = iota + 1
)

// Room maintains the set of active clients and broadcasts messages to the
// clients.
type Room struct {
	manager *RoomManager

	// Registered clients.
	clients map[Username]*Client

	// Inbound messages from the clients.
	toRoom chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	roomId RoomId

	roomState RoomState
}

func newRoom(manager *RoomManager, roomId RoomId) *Room {
	return &Room{
		manager:    manager,
		toRoom:     make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[Username]*Client),
		roomId:     roomId,
		roomState:  WaitingForPlayers,
	}
}

func getRoomId(r *http.Request) (RoomId, error) {
	return RoomId(r.URL.Path), nil
}

func (r *Room) run() {
	defer func() {
		r.manager.unregister <- r
	}()
	for {
		select {
		case client := <-r.register:
			r.handleRegister(client)
		case client := <-r.unregister:
			r.handleUnregister(client)
			if len(r.clients) == 0 {
				// trigger the defer to unregister
				return
			}
		case message := <-r.toRoom:
			r.handleMessage(message)
		}
	}
}

func (r *Room) handleRegister(client *Client) {
	r.clients[client.Username] = client
}

func (r *Room) handleUnregister(client *Client) {
	if _, ok := r.clients[client.Username]; ok {
		delete(r.clients, client.Username)
		close(client.send)
	}
}

func (r *Room) handleMessage(message []byte) {
	for _, client := range r.clients {
		select {
		case client.send <- message:
		default:
			close(client.send)
			delete(r.clients, client.Username)
		}
	}
}
