package socket

import (
	"bytes"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

type Username string

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type ClientState int

// Client states enums
const (
	NameCreation ClientState = iota + 1
	InLobby
	InGame
)

// Client is a middleman between the websocket connection and the room.
type Client struct {
	room *Room

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte

	Username Username

	clientState ClientState
}

// readPump pumps messages from the websocket connection to the room.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	logFields := log.WithFields(log.Fields{
		"package": "socket",
		"topic":   "readPump",
	})

	defer func() {
		c.room.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	err := c.conn.SetReadDeadline(time.Now().Add(pongWait))
	if err != nil {
		logFields.Errorf("error setting the read deadline")
		return
	}
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				logFields.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		c.room.toRoom <- message
	}
}

// writePump pumps messages from the room to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The room closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func ServeWs(manager *RoomManager, w http.ResponseWriter, r *http.Request) {
	logFields := log.WithFields(log.Fields{
		"package": "socket",
		"topic":   "serveWs",
	})
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logFields.Errorf("unable to upgrade websocket: %v", err)
		return
	}

	roomId, err := getRoomId(r)
	if err != nil {
		logFields.Println(err)
		return
	}

	room := manager.rooms[roomId]
	for _, debugRoom := range manager.rooms {
		logFields.Infof("Current Rooms:%v", debugRoom.roomId)
	}
	if room == nil {
		logFields.Infof("Creating a new room: %v", roomId)
		room = newRoom(manager, roomId)
		manager.register <- room
		go room.run()
	}

	createClient(room, conn)
}

var usernameInt = 0

func createClient(room *Room, conn *websocket.Conn) {
	logFields := log.WithFields(log.Fields{
		"package": "socket",
		"topic":   "createClient",
	})
	logFields.Debugf("Creating Client: %v", usernameInt)
	client := &Client{
		room:        room,
		conn:        conn,
		send:        make(chan []byte, 256),
		Username:    Username(usernameInt),
		clientState: NameCreation,
	}
	usernameInt++
	client.room.register <- client
	go client.writePump()
	go client.readPump()
}
